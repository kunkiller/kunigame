
interface IMath {
	rdirect: Function // 随机正负1
}

interface IEvents {
  reset: Function // 重置状态
  addOnce: Function // 一次性事件
}